#include <Arduino.h>
#include <stdlib.h>
#include <DallasTemperature.h>
#include <OneWire.h>

#define ONE_WIRE_PIN 8
#define TIME_BETWEEN_READINGS 2000  // Amount of time between reading the next set of temperature values for all the sensors (in ms)

OneWire wire(ONE_WIRE_PIN);
uint16_t numSwitches;
uint16_t numTempSensors;
uint16_t index;
DallasTemperature sensors(&wire);
uint8_t numDevices;
DeviceAddress* addresses;

void setup()
{
	Serial.begin(9600);
	sensors.begin();

	//count the number of devices
	numDevices = sensors.getDeviceCount();
	Serial.print(numDevices);
	Serial.println(" devices found on the 1-Wire bus");

	//get all the addresses that exist on the bus
	addresses = (DeviceAddress*)malloc(numDevices*sizeof(DeviceAddress));
	
	for (int i = 0; i < numDevices; i++)
	{
		if (!sensors.getAddress((uint8_t*)&addresses[i], i))
		{
			Serial.print("No device was found at index: ");
			Serial.println(i);
		}
	}

}

void loop()
{
	readAllTemperatures();
	Serial.println("");
	delay(TIME_BETWEEN_READINGS);
}

void readAllTemperatures()
{
	float temp;
	sensors.setWaitForConversion(true);
	sensors.requestTemperatures(); //sends the command for all sensors to start the conversion
	for (int i = 0; i < numDevices; i++)
	{
		temp = sensors.getTempC(addresses[i]);
		printAddress(addresses[i]);
		Serial.print(": ");

		if (temp != DEVICE_DISCONNECTED)
		{
			Serial.print(temp);
			Serial.println(" Degrees C");
		}

		else
		{
			Serial.println("Device Disconnected");
		}
	}
}

void printAddress(DeviceAddress addr)
{
	for (int i = 0; i < 8; i++)
	{
		Serial.print(addr[i], HEX);
		Serial.print(" ");
	}
}
